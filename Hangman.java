/*
* Assignment 2 2023 - 110
*
* runs a Hangman game
* asks Player 1 for a 4-letter word
* asks Player 2 to make guess letters
* game keeps going until answered is revealed
* or game over with 6 mistakes
*
* author: Jennifer Huang
* ID: 1437051
*/

import java.util.Scanner;


public class Hangman {


	public static void main (String[] args){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		
		System.out.println("Player 1 enter a word");
		String answer = reader.nextLine();
		
		
		//checks if word is 4 letters
		while (answer.length()>4 || answer.length()<4){
			System.out.println(
			"**Please enter a word with 4 letters**");
			answer = reader.nextLine();
		}
		
		runGame(answer);
	}
	
	
	
	/*
	* method to determine if guessed letter is in answer
	* 
	* takes answer & guesses, converts them both into upper case
	* compares each letter of the answer to the guess
	* if they match, return an integer
	* if no matches, return -1
	*/
	
	public static int isLetterInWord(String answer, char guess){
		
		char guessUpperCase = toUpperCase(guess);
		
		
		//idx represents the letter position (index)
		for (int idx = 0; idx < answer.length(); idx++){
			
			char answerUpperCase = 
				toUpperCase(answer.charAt(idx));
			
			if (guessUpperCase == answerUpperCase){
				return idx;
			}
			
		}
		return -1;
	}
	
	
	
	/*
	* method to turn any characters to upper case letters
	*
	* if lower case, go back 32 values in unicode array
	* and returns char
	* if upper case letter, return as is
	*/
	
	public static char toUpperCase(char chr){
	
		if (chr >= 'a' && chr <= 'z'){
			chr -= 32;
			return chr;
		}
		return chr;
	}
	
	
	
	/*
	* method to display progress of the game
	* 
	* checks every position for boolean
	* if true, reveals letter
	* if false, displays a blank
	*/
	
	public static void printWork(String answer, boolean[] letterPosStatus){
		
		char[] blanks = new char[] {'_','_','_','_'};
		
		System.out.print("Your result is: ");
		
		
		for (int i = 0; i < letterPosStatus.length; i++){
			
			if (!letterPosStatus[i]) {
				System.out.print(blanks[i]);
				
			} else {
				System.out.print(answer.charAt(i));
			
			}
		}
		System.out.println("");
	}
	
	
	
	/*
	* asks Player 2 to guess a letter
	* checks if letter is in word
	* counts every correct or wrong guesses
	* if right 4 times, displays "You win!"
	* if wrong 6 times, displays "Better luck next time."
	*/
	
	public static void runGame(String answer){
		
		int correct = 0, misses = 0;
		
		//letterIdxStatus is short for letter position status
		boolean[] letterIdxStatus = new boolean[] 
			{false, false, false, false};
		
		
		//loop to repeat guess attempts
		while (correct < 4 && misses < 6) {
			java.util.Scanner reader = new java.util.Scanner(System.in);
			
			System.out.println("Player 2 guess a letter");
			char guess = reader.next().charAt(0);
			
			int letterIndex = isLetterInWord(answer, guess);
			
			if (letterIndex >= 0 && letterIndex <= 3) {
				
				letterIdxStatus[letterIndex] = true;
				correct++;
			
			} else {
				misses++;
			
			}
			printWork(answer, letterIdxStatus);
		}
		
		
		//displays result message game is over
		if (misses == 6){
			System.out.println("The answer is " + answer);
			System.out.println("Better luck next time.");
		
		} else {
			System.out.println("You win!");
		} 
	}
}
